
lazy val commonSettings = Seq(
    name := "akka-cassandra",
    scalaVersion := "2.12.1",
    version := "0.1.0-SNAPSHOT"
)

lazy val root = (project in file(".")).settings(commonSettings)

addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)

val akkaVersion = "2.4.17"
val akkaHttpVersion = "10.0.5"

libraryDependencies ++= Seq(
  "com.typesafe.akka"     %% "akka-stream-kafka"    % "0.13",
  "com.typesafe.akka"     %% "akka-stream"          % akkaVersion,
  "com.typesafe.akka"     %% "akka-stream-testkit"  % akkaVersion,

  //akka-casssandra
  "com.typesafe.akka"    %% "akka-http"             % akkaHttpVersion,
  "com.typesafe.akka"    %% "akka-http-testkit"     % akkaHttpVersion   % "test",
  "ch.qos.logback"        % "logback-classic"       % "1.1.9",
  "com.outworkers"        % "phantom-dsl_2.12"      % "2.6.1",
  "org.scala-lang"        % "scala-reflect"         % "2.12.1",
  "com.github.fomkin"    %% "pushka-json"           % "0.8.0"
)
    