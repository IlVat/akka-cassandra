package user.management

import scala.language.postfixOps
import scala.util.{Failure, Success}

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

import com.outworkers.phantom.dsl.context
import user.management.Config.General._
import user.management.cassandra.MyDatabase
import user.management.http.server.{AkkaCommon, UserRoutes}

object Boot extends App with AkkaCommon with UserRoutes {
  override implicit val system = ActorSystem(interface)
  override implicit val materializer = ActorMaterializer()

  val server = Http().bindAndHandle(userRoutes, interface = interface, port = port)

  server onComplete  {
    case Success(x) =>
      println(s"Server successfully bound to $interface: $port")
      MyDatabase.createAsync
    case Failure(ex) => println(ex.getMessage)
  }
}
