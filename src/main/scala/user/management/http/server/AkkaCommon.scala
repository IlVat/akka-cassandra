package user.management.http.server

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

trait AkkaCommon {
  implicit val system: ActorSystem
  implicit  val materializer: ActorMaterializer
}