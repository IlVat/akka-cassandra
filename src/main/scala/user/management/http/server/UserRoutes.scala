package user.management.http.server

import java.util.UUID

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

import akka.http.scaladsl.server.Directives._

import pushka.json._
import user.management.abstractions.EntityManagement
import user.management.cassandra.{MyDatabase, User}
import user.management.helpers.EntityUnmarshaller
import user.management.models.Response


trait UserRoutes extends UserManagement with AkkaCommon {
  implicit val um = EntityUnmarshaller.create[User]

  val userRoutes = pathPrefix("users") {
    path("create") {
      post {
        entity(as[User]) { user =>
          onComplete(create(user)) {
            case Success(x) => complete(write(Response.created(x)))
            case Failure(ex) => complete(write(Response.forbidden))
          }
        }
      }
    }
  }
}


trait UserManagement extends EntityManagement[UUID, User] {
  override def create(d: User): Future[UUID] = {
    MyDatabase.usersModel.create(d)

  }

  override def get(id: UUID): Future[User] = ???

  override def getAll: Future[Set[User]] = ???

  override def update(d: User): Future[UUID] = ???

  override def delete(id: UUID): Future[UUID] = ???
}




