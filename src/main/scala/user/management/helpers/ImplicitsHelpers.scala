package user.management.helpers

import org.joda.time.DateTime

object ImplicitsHelpers {
  implicit def long2DateTime(date: Long): DateTime = new DateTime(date)

}
