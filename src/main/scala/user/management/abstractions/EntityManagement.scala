package user.management.abstractions


import scala.Specializable.Group
import scala.concurrent.Future
import scala.language.implicitConversions

import user.management.models.Response




class UUID(val uuid: java.util.UUID)
object UUID

object Implicits {
  implicit def javaUUID2scalaUUID(uuid: java.util.UUID): UUID = {
    new UUID(uuid)
  }
}

trait EntityManagement[@specialized(new Group((Int, Long, UUID))) ID, T] {
  def create(d: T): Future[ID]
  def get(id: ID): Future[T]
  def getAll: Future[Set[T]]
  def update(d: T): Future[ID]
  def delete(id: ID):Future[ID]
}
