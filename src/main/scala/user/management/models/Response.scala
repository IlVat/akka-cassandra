package user.management.models

import pushka.annotation.pushka


@pushka case class Message(msg: String)
@pushka case class Response[T](code: Int, msg: Message,  entity: Option[T] = None)

object Response {
  val answerOk = Response[String](200, Message("OK"))
  val forbidden = Response[String](403, Message("Forbidden"))
  val errorAnswer: String => Response[String] = x => Response(400, Message(x))
  val updateError = errorAnswer("Update Error")

  def created[A](entity: A) = Response[A](201, Message("Created"), Some(entity))
  def Ok[A](entity: A) = Response[A](200, Message("OK"), Some(entity))
}