package user.management

import java.net.InetAddress

import com.typesafe.config.ConfigFactory

object Config {
  private val rootConfig = ConfigFactory.load()
  val localAddress = InetAddress.getLocalHost.getHostAddress

  object General {
    private val generalConf = rootConfig.getConfig("general")
    val interface = generalConf.getString("interface")
    val port = generalConf.getInt("port")
    val system = generalConf.getString("system")
  }
}
