package user.management.cassandra

import com.datastax.driver.core.{Cluster, SocketOptions}
import com.outworkers.phantom.dsl._

object CassandraConnector {
  // This is a fictional series of IP addresses
  val hosts = Seq("127.0.0.1")
  val port = 9042
  val connector = ContactPoints(hosts, port).withClusterBuilder(socketOptions).noHeartbeat().keySpace("my_app")

  private def socketOptions(builder: Cluster.Builder) = {
    builder.withSocketOptions(new SocketOptions().setConnectTimeoutMillis(10000))
  }
}

