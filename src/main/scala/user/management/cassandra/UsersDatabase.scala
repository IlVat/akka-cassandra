package user.management.cassandra

import com.outworkers.phantom.dsl._

class UsersDatabase(override val connector: KeySpaceDef) extends Database[UsersDatabase](connector) {
  object usersModel extends ConcreteUsers with connector.Connector
}

object MyDatabase extends UsersDatabase(CassandraConnector.connector)
