package user.management.cassandra

import java.util.UUID

import scala.concurrent.Future

import com.outworkers.phantom.dsl._
import pushka.annotation._

@pushka
case class User(
    id: UUID = UUID.randomUUID(),
    email: String,
    name: String,
    registrationDate: Long = System.currentTimeMillis()
)

class Users extends CassandraTable[Users, User] {

  object id extends UUIDColumn(this) with PartitionKey {
    override def name = "user_id"
  }

  object email extends StringColumn(this)

  object name extends StringColumn(this)

  object registrationDate extends DateTimeColumn(this) with ClusteringOrder {
    override def name = "registration_date"
  }

  override def fromRow(r: Row): User = {
    User(id(r),
      email(r),
      name(r),
      registrationDate(r).getMillis)
  }

}

abstract class ConcreteUsers extends Users with RootConnector {

  import user.management.helpers.ImplicitsHelpers.long2DateTime

  def create(user: User): Future[UUID] = {
    insert
      .value(_.id, user.id)
      .value(_.email, user.email)
      .value(_.name, user.name)
      .value[DateTime](_.registrationDate, user.registrationDate)
      .consistencyLevel_=(ConsistencyLevel.ALL)
      .future()
      .map(_ => user.id)
  }

  def getById(id: UUID): Future[Option[User]] = {
    select.where(_.id eqs id).one
  }
}

