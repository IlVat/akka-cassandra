import java.util.UUID

import pushka.annotation.pushka
import _root_.pushka.Writer
import _root_.pushka.json._

@pushka
case class User(
    id: UUID = UUID.randomUUID(),
    email: String,
    name: String,
    registrationDate: Long = System.currentTimeMillis()
)

val json = """{"email": "hello@mail.com", "name": "alis"}"""

read[User](json)

User(email = "fdg", name = "sg")